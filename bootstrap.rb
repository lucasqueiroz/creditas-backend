require 'require_all'
require 'faker'

require_all 'lib'

def clear_screen
  puts `clear`
end

def random_products
  products = []
  rand(1..2).times do
    name = Faker::Name.name
    price = rand(100)
    products << Product.new(name, price)
  end
  rand(1..2).times do
    name = Faker::Name.name
    price = rand(100)
    products << Book.new(name, price)
  end
  rand(1..2).times do
    name = Faker::Name.name
    price = rand(100)
    products << DigitalMedia.new(name, price)
  end
  rand(1..2).times do
    name = Faker::Name.name
    price = rand(100)
    products << Membership.new(name, price)
  end
  products
end

def build_customer
  puts "Let's start by creating your account!"

  print "What's your name? "
  name = gets.chomp

  print "What's your email address? "
  email = gets.chomp

  address = build_address

  Customer.new(name, email, address)
end

def build_address
  print "What's your address (street name)? "
  street = gets.chomp

  print "What's the number of your building? "
  number = gets.chomp

  print "What's your city? "
  city = gets.chomp

  print "What's your state? "
  state = gets.chomp

  print "What's your country? "
  country = gets.chomp

  print "What's your zipcode? "
  zip_code = gets.chomp

  Address.new(street, number, city, state, country, zip_code)
end

clear_screen
puts 'Hello Customer!'

customer = build_customer

def build_cart_message(order)
  total = '%.2f' % order.total
  "You have #{order.products.size} items in your cart with a total of R$ #{total}"
end

def build_products_message(products)
  products.collect do |product_key, product_value|
    name = product_value.name
    price = '%.2f' % product_value.price
    type = product_value.class.name
    "#{product_key} - #{name} - R$ #{price} - #{type}\n"
  end.join
end

def build_order_messages(order, products)
  puts build_cart_message(order)
  puts ''
  puts build_products_message(products)
  puts ''
  print 'Select a product (by id): '
  id = gets.chomp.to_i
  until products.include?(id)
    puts 'Product not found.'
    print 'Select a product (by id): '
    id = gets.chomp.to_i
  end
  order.add_product(products[id])
  puts 'Item added!'
end

clear_screen
puts "Hello, #{customer.name}!"
puts ''

continue = true

while continue
  order = Order.new(customer)
  finished_order = false

  products = random_products.map.with_index.to_h.invert

  until finished_order
    build_order_messages(order, products)
    print 'Continue order? [y/n] '
    finished_order = gets.chomp == 'n'
  end

  total = '%.2f' % order.total
  puts "Total: R$ #{total}"

  if customer.has_coupon_code?
    print 'Do you want to use your coupon? [y/n] '
    use_coupon = gets.chomp == 'y'
    order.apply_coupon
    total = '%.2f' % order.total
    puts "New total: R$ #{total}"
  end

  puts 'Paying order...'
  order.pay

  puts 'Order paid!'
  print 'Create another order? [y/n] '
  continue = gets.chomp != 'n'
end
