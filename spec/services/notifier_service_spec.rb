RSpec.describe NotifierService do
  let(:notification) { build(:notification) }

  context 'when notification is sent' do
    before do
      ignore_puts
      NotifierService.notify_customer(notification)
    end

    it 'sends email to customer' do
      # Notifier Service should handle sending emails
    end

    it 'updates customer' do
      expect(notification.customer).to be_notified
    end
  end
end
