FactoryBot.define do
  factory :customer do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    address

    initialize_with { new(name, email, address) }
  end
end
