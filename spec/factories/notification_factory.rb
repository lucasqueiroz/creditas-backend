FactoryBot.define do
  factory :notification do
    customer
    subject { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraph }

    initialize_with { new(customer, subject, body) }
  end
end
