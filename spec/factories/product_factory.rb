FactoryBot.define do
  factory :product do
    name { Faker::Name.name }

    initialize_with { new(name) }
  end
end
