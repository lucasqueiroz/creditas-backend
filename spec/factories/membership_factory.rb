FactoryBot.define do
  factory :membership do
    name { Faker::Name.name }

    initialize_with { new(name) }
  end
end
