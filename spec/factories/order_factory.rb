FactoryBot.define do
  factory :order do
    customer

    initialize_with { new(customer) }
  end
end
