FactoryBot.define do
  factory :book do
    name { Faker::Name.name }

    initialize_with { new(name) }
  end
end
