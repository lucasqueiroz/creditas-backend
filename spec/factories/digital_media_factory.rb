FactoryBot.define do
  factory :digital_media do
    name { Faker::Name.name }

    initialize_with { new(name) }
  end
end
