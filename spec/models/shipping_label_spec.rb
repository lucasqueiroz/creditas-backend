RSpec.describe ShippingLabel do
  let(:order) { build(:order) }

  context 'when paying order' do
    let(:product) { build(:product) }

    before do
      order.add_product(product)
      order.pay
    end

    it 'prints shipping label' do
      expect(product.shipping_label).to be_printed
    end
  end
end
