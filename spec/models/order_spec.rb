RSpec.describe Order do
  let(:order) { build(:order) }

  context 'when paying order' do
    let(:product) { build(:product) }

    before do
      order.add_product(product)
    end

    it 'receives confirm_order' do
      expect(product).to receive(:confirm_order)
      order.pay
    end
  end
end
