RSpec.describe Membership do
  let(:order) { build(:order) }
  let(:membership) { build(:membership) }

  before do
    order.add_product(membership)
  end

  context 'when membership is paid' do
    before do
      ignore_puts
      order.pay
    end

    it 'enabled membership' do
      expect(membership).to be_enabled
    end

    it 'sends email to customer' do
      expect(order.customer).to be_notified
    end
  end
end
