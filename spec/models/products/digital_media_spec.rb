RSpec.describe DigitalMedia do
  let(:order) { build(:order) }
  let(:digital_media) { build(:digital_media) }

  before do
    order.add_product(digital_media)
  end

  context 'when digital media is paid' do
    before do
      ignore_puts
      order.pay
    end

    it 'sends email to customer' do
      expect(order.customer).to be_notified
    end

    it 'sends coupon code' do
      expect(order.customer).to have_coupon_code
    end
  end
end
