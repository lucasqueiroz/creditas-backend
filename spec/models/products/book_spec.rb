RSpec.describe Book do
  let(:order) { build(:order) }
  let(:book) { build(:book) }

  before do
    order.add_product(book)
  end

  context 'when book is paid' do
    before do
      ignore_puts
      order.pay
    end

    it 'generates shipping label' do
      expect(book.shipping_label).not_to be_nil
    end

    it 'has correct address in shipping label' do
      expect(book.shipping_label.address).to eq(order.customer.address)
    end

    it 'is tax-exempt' do
      expect(book.shipping_label).to be_tax_exempt
    end
  end
end
