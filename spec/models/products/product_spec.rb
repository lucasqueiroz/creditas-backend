RSpec.describe Product do
  let(:order) { build(:order) }
  let(:product) { build(:product) }

  before do
    order.add_product(product)
  end

  context 'when product is paid' do
    before do
      ignore_puts
      order.pay
    end

    it 'generates shipping label' do
      expect(product.shipping_label).not_to be_nil
    end

    it 'has correct address in shipping label' do
      expect(product.shipping_label.address).to eq(order.customer.address)
    end
  end
end
