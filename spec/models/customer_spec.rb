RSpec.describe Customer do
  let(:customer) { build(:customer) }

  context 'when using coupon' do
    before do
      ignore_puts
      customer.give_coupon_code(CouponCode.new(10))
      customer.use_coupon
    end

    it 'removes coupon' do
      expect(customer).not_to have_coupon_code
    end
  end
end
