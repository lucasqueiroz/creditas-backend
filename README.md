# Creditas Backend Challenge [![pipeline status](https://gitlab.com/lucasqueiroz/creditas-backend/badges/master/pipeline.svg)](https://gitlab.com/lucasqueiroz/creditas-backend/commits/master)

This challenge consists on a revamp of a payments software.
The software needs to be refactored so the maintenance time and effort can be reduced.

## Running the project

Make sure to have `ruby` installed and updated (Tested on 2.6.3)
1. Run `ruby bootstrap.rb` on the root folder of the project

## Running tests

1. Run `rspec`

## Business Rules

- If the payment is for a physical item, a `shipping label` must be printed, so it can be placed on the box for the shipment.
- If the payment is for a membership, the membership must be enabled and the customer should be notified through their email address.
- If the payment is for a regular book, the `shipping label` should include a notification that the product is tax-exempt.
- If the payment is for a digital media, the buyer should receive an email with the description and also a coupon code for R$ 10,00 off.

## Technologies

1. [Ruby](https://www.ruby-lang.org/en/)
2. [RSpec](https://rspec.info/) is used to run unit tests
3. [GitLabCI](https://docs.gitlab.com/ee/ci/) is used for continuous integration
4. [FactoryBot](https://github.com/thoughtbot/factory_bot) is used to generate fixtures
5. [Faker](https://github.com/faker-ruby/faker) is used to generate fake data on tests
6. [GitLab/Git](https://gitlab.com/) is used for version control

## Solution

This solution uses models and services to handle the business rules.
The types of products inherit the [Product](https://gitlab.com/lucasqueiroz/creditas-backend/blob/master/lib/models/products/product.rb) class, avoiding *if/else statements* and *switch/case blocks*.

## Flow

1. Product is created
    - [Product](https://gitlab.com/lucasqueiroz/creditas-backend/blob/master/lib/models/products/product.rb) - Physical item that will be delivered (Prints a shipping label)
    - [Membership](https://gitlab.com/lucasqueiroz/creditas-backend/blob/master/lib/models/products/membership.rb) - Membership that will be enabled and send an email to the customer
    - [Book](https://gitlab.com/lucasqueiroz/creditas-backend/blob/master/lib/models/products/book.rb) - Item that will be delivered tax-exempt (Prints a shipping label)
    - [Digital Media](https://gitlab.com/lucasqueiroz/creditas-backend/blob/master/lib/models/products/digital_media.rb) - Digital item that will be sent through email and add a coupon code to the customer
2. Customer is created
3. Order is created
    - An order can have multiple Products (classes that inherit from Product)
    - An order has a single customer
4. Coupon code is applied, if customer have one and if they wish to apply
5. Order is paid
6. Items that require a shipping label will have it printed
7. Items that send an email as a result, will send the email
