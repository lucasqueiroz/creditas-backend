class Notification
  attr_reader :customer, :subject, :body

  def initialize(customer, subject, body)
    @customer = customer
    @subject = subject
    @body = body
  end

  def notify
    NotifierService.notify_customer(self)
  end
end
