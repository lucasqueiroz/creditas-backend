class ShippingLabel
  attr_reader :customer, :address, :tax_exempt, :printed

  def initialize(customer, address)
    @customer = customer
    @address = address
    @tax_exempt = false
    @printed = false
  end

  alias_method 'tax_exempt?', :tax_exempt

  def tax_exempt
    @tax_exempt = true
  end

  alias_method 'printed?', :printed

  def print
    # Sends label to printer with the address information
    # Output to STDOUT is a mock of a label
    puts 'Recipient:'
    puts @customer.name
    puts "#{@address.number} #{@address.street}"
    puts "#{@address.city} - #{@address.state}"
    puts @address.country
    puts @address.zip_code
    tax_exempt = @tax_exempt ? 'Yes' : 'No'
    puts "Tax-exempt: #{tax_exempt}"
    @printed = true
  end
end
