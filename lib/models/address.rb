class Address
  attr_reader :street, :number, :city, :state, :country, :zip_code

  def initialize(*address)
    @street, @number, @city, @state, @country, @zip_code = address
  end
end
