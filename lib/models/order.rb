class Order
  attr_reader :products, :customer, :paid, :coupon_code

  def initialize(customer, products = [])
    @products = []
    @customer = customer
  end

  def add_product(product)
    raise ArgumentError, 'Product is invalid' unless product.is_a?(Product)

    @products << product
  end

  def pay
    raise OrderAlreadyPaidError if @paid

    @paid = true
    @products.each do |product|
      product.confirm_order(self)
    end
  end

  def apply_coupon
    @coupon_code = customer.coupon_code
    customer.use_coupon
  end

  def total
    total = @products.map(&:price).inject(0, &:+)
    total -= @coupon_code.value if @coupon_code
    total = 0 if total < 0
    total
  end
end
