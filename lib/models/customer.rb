class Customer
  attr_reader :name, :email, :address, :notified, :coupon_code

  def initialize(name, email, address)
    @name = name
    @email = email
    @address = address
    @notified = false
  end

  alias_method 'notified?', :notified

  def notified
    @notified = true
  end

  def give_coupon_code(coupon_code)
    @coupon_code = coupon_code
  end

  alias_method 'has_coupon_code?', :coupon_code

  def use_coupon
    @coupon_code = nil
  end
end
