require_relative 'product'

class DigitalMedia < Product
  SUBJECT = "Digital Media Purchased!".freeze

  def confirm_order(order)
    notify_customer(order.customer)
    create_coupon_code(order.customer)
  end

  private

  def notify_customer(customer)
    body = "The #{name} media was purchased successfully! Thanks!"
    notification = Notification.new(customer, SUBJECT, body)
    notification.notify
  end

  def create_coupon_code(customer)
    coupon_code = CouponCode.new(10)
    customer.give_coupon_code(coupon_code)
  end
end
