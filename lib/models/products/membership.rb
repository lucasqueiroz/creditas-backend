require_relative 'product'

class Membership < Product
  attr_reader :enabled

  SUBJECT = "Membership Enabled!".freeze

  def initialize(name = '', price = 0)
    super(name, price)
    @enabled = false
  end

  def confirm_order(order)
    @enabled = true
    notify_customer(order.customer)
  end

  alias_method 'enabled?', :enabled

  private

  def notify_customer(customer)
    body = "The #{name} membership was enabled successfully! Thank you for your purchase"
    notification = Notification.new(customer, SUBJECT, body)
    notification.notify
  end
end
