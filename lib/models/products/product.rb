require 'securerandom'

class Product
  attr_reader :uuid, :name, :shipping_label, :price

  def initialize(name = '', price = 0)
    @id = SecureRandom.uuid
    @name = name
    @price = price
  end

  def confirm_order(order)
    create_shipping_label(order.customer)
    @shipping_label.print
  end

  private

  def create_shipping_label(customer)
    @shipping_label = ShippingLabel.new(customer, customer.address)
  end
end
