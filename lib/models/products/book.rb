require_relative 'product'

class Book < Product
  private

  def create_shipping_label(customer)
    super(customer)
    @shipping_label.tax_exempt
  end
end
