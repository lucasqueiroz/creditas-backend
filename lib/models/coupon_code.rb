class CouponCode
  attr_reader :id, :value

  def initialize(value)
    @id = SecureRandom.hex
    @value = value
  end
end
