class NotifierService
  class << self
    def notify_customer(notification)
      customer = notification.customer
      subject = notification.subject
      body = notification.body
      # Sends email to #{notification.customer.email} with #{notification.body} as content
      # Output to STDOUT is a mock of an email
      puts "Recipient: #{customer.email}"
      puts "Subject: #{subject}"
      puts "#{body}"
      customer.notified
    end
  end
end
